/***************************************************************************
 *   Copyright (C) 2008 by Lisandro Damián Nicanor Pérez Meyer             *
 *   perezmeyer@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301  USA                                                   *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QPointF>
#include "ui_mainwindowqzplot.h"

/// The main window
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow();

public slots:
	/// Gets data from the UI when the Add value button is pressed
	void addData();

	/// Change the normal impedance
	void changeNormalImpedance();

	/// Draws the data in the Smith Chart
	void drawData();

private:
	/// The user interface
	Ui::MainWindow ui;

	/// The list of impedance values
	/**
	This list is represented by QPointF objects. In x we will hold the real part
	of the complex number and in y the imaginary one.
	*/
	QVector<QPointF> impedanceValues;

	/// Value of the normal impedance
	qreal normalImpedance;
};

#endif //MainWindow

