/***************************************************************************
 *   Copyright (C) 2008 by Lisandro Damián Nicanor Pérez Meyer             *
 *   perezmeyer@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA  02110-1301  USA                                                   *
 ***************************************************************************/
#include "mainwindow.h"

#include <QString>

void MainWindow::addData()
{
	QString realStr(""), imaginaryStr("");
	qreal real(0.0), imaginary(0.0);

	realStr = ui.realImpedanceLineEdit->displayText();
	imaginaryStr = ui.imaginaryImpedanceLineEdit->displayText();

	real = realStr.toDouble();
	imaginary = imaginaryStr.toDouble();

	ui.dataPlainTextEdit->appendPlainText(realStr + " + j " + imaginaryStr);

	impedanceValues.append(QPointF(real, imaginary));

	drawData();
}

void MainWindow::drawData()
{
	for(int i = 0; i < impedanceValues.size(); i++)
	{
		ui.smithChart->setData(impedanceValues.at(i).x() / normalImpedance,
		                       impedanceValues.at(i).y() / normalImpedance);
	}
	update();
}

void MainWindow::changeNormalImpedance()
{
	normalImpedance = ui.normalImpedanceLineEdit->displayText().toDouble();

	ui.smithChart->clear();

	if(normalImpedance == 0.0)
	{
		normalImpedance = 50.0;
		ui.normalImpedanceLineEdit->setText("50.0");
	}

	drawData();
}

MainWindow::MainWindow()
{
	ui.setupUi(this);

	normalImpedance = 50.0;

	// Connections
	// Impedance add button
	connect(ui.addImpedancePushButton, SIGNAL(clicked()),
	        this, SLOT(addData()));
	// Normal impedance change
	connect(ui.normalImpedanceLineEdit, SIGNAL(editingFinished()),
	        this, SLOT(changeNormalImpedance()));
}
