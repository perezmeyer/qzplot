UI_DIR = ui

QT += core gui widgets

SOURCES += main.cpp \
	smithchart.cpp \
 ui/mainwindow.cpp

HEADERS += smithchart.h \
 ui/mainwindow.h

# Sub-directories
UI_DIR = ../qt/uic
MOC_DIR = ../qt/moc
OBJECTS_DIR = ../qt/obj
RCC_DIR = ../qt/res


TEMPLATE = app
CONFIG += warn_on \
	  thread \
          qt

TARGET = qzplot
DESTDIR = ../bin/

unix {
    CONFIG += debug
    # Prefix: base instalation directory
    isEmpty( PREFIX ){
        PREFIX = /usr/local
    }

    DEB_BUILD = $$system(echo \$DEB_BUILD_OPTIONS)
    contains(DEB_BUILD, nostrip){
        QMAKE_STRIP =:
    }

    DEFINES += PREFIX=\\\"$${PREFIX}\\\"
    target.path = $${PREFIX}/bin
}

win32 {
    CONFIG += release
    INCLUDEPATH = .
}
FORMS = ui/mainwindowqzplot.ui


