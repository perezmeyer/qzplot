; General stuff
; -------------

!define VERSION "0.0.1"

Name "QZPlot"
OutFile "..\..\bin\qzplot-${VERSION}.exe"

InstallDir $PROGRAMFILES\QZPlot

; Preparing
; ---------

!include "MUI.nsh"
!define MUI_ABORTWARNING
Var STARTMENU_FOLDER
var MUI_TEMP

; Pages
; -----

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "..\..\COPYING"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

; Localization
; ------------

!insertmacro MUI_LANGUAGE "English"
LangString DESC_SecQZPlot	${LANG_ENGLISH} "The main QZPlot application itself."

; Sections
; --------

Section "-QZPlot" SecQZPlot
	SetOutPath $INSTDIR

	FILE "..\..\bin\qzplot.exe"
	FILE "..\..\bin\mingwm10.dll"

	WriteUninstaller "$INSTDIR\uninstall.exe"

	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
		CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
		CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\QZPlot.lnk" "$INSTDIR\QZPlot.exe"
		WriteINIStr "$SMPROGRAMS\$STARTMENU_FOLDER\QZPlot Website.url" "InternetShortcut" "URL" "http://qzplot.sourceforge.net/"
		CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\uninstall.exe"
	!insertmacro MUI_STARTMENU_WRITE_END
	
	WriteRegExpandStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "UninstallString" '"$INSTDIR\uninstall.exe"'
	WriteRegExpandStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "InstallLocation" "$INSTDIR"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "DisplayName" "QZPlot"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "DisplayIcon" "$INSTDIR\QZPlot.exe,0"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "DisplayVersion" "${VERSION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "URLInfoAbout" "http://qzplot.sourceforge.net/"
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "NoModify" "1"
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot" "NoRepair" "1"

SectionEnd 

; Uninstaller
; -----------

Section "Uninstall"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZPlot"

	Delete "$INSTDIR\QZPlot.exe"

	Delete "$INSTDIR\Uninstall.exe"
	RMDir "$INSTDIR\examples"
	RMDir "$INSTDIR"

  	!insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
    
  	Delete "$SMPROGRAMS\$MUI_TEMP\QZPlot.lnk"
	Delete "$SMPROGRAMS\$MUI_TEMP\QZPlot Website.url"
	Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"
	RMDir "$SMPROGRAMS\$MUI_TEMP"
SectionEnd

; Description
; -----------

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${SecQZPlot} $(DESC_SecQZPlot)
	!insertmacro MUI_DESCRIPTION_TEXT ${SecNEC} $(DESC_SecNEC)
	!insertmacro MUI_DESCRIPTION_TEXT ${SecLocal} $(DESC_SecLocal)
	!insertmacro MUI_DESCRIPTION_TEXT ${SecExample} $(DESC_SecExample)
!insertmacro MUI_FUNCTION_DESCRIPTION_END
